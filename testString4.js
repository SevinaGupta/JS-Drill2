//import { fullName } from "./string4.mjs";

const fullName = require("./string4");


const name1 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

let result = fullName({"middle_name": "doe","first_name": "JoHN", "last_name": "SMith"}) //return full name
let result1 = fullName({"first_name": "JoHN", "last_name": "SMith"}) // return full name
let result2 = fullName({}) // return invalid string
console.log(result)
console.log(result1)
console.log(result2)
