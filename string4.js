function getName(name1)
{
let full_name = []
if(name1){
 
	let f_name = name1["first_name" ]
	let l_name = name1["last_name"]
	let m_name = name1["middle_name"]

	if( f_name != undefined && m_name != undefined && l_name != undefined)
	{
		full_name = f_name+" "+m_name+" "+l_name
	}
	else if( f_name != undefined && m_name == undefined && l_name != undefined)
	{
		full_name = f_name+" "+l_name
	}
	else if( f_name != undefined && m_name != undefined && l_name == undefined)
	{
		full_name = f_name+" "+m_name
	}
	else if( f_name == undefined && m_name != undefined && l_name != undefined)
	{
		full_name = m_name+" "+l_name
	}
	else if( f_name != undefined && m_name == undefined && l_name == undefined)
	{
		full_name = f_name
	}
	else if( f_name == undefined && m_name != undefined && l_name == undefined)
	{
		full_name = m_name
	}
	else if( f_name == undefined && m_name == undefined && l_name != undefined)
	{
		full_name = l_name
	}
	else 
	{
		return 'invalid name'
	}

	 return full_name
}

}
module.exports = getName;


